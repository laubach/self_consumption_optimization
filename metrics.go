package main

import (
	"bitbucket.org/laubach/self_consumption_optimization/cfg"
	"bitbucket.org/laubach/self_consumption_optimization/pkg/fronius"
	weatherYr "bitbucket.org/laubach/self_consumption_optimization/pkg/norwegian"
	weather "bitbucket.org/laubach/self_consumption_optimization/pkg/openweather"
	switchablesocket "bitbucket.org/laubach/self_consumption_optimization/pkg/switch"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
	log "github.com/sirupsen/logrus"
	"math"
	"strconv"
	"time"
)

var (
	namespace        = "fronius_optimiser"
	batteryNamespace = "battery_optimiser"
	weatherNameSpace = "weather_optimiser"
	switchNameSpace  = "switch"

	scrapeDurationGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "scrape_duration_seconds",
		Help:      "Time it took to scrape the device in seconds",
	})
	scrapeErrorCount = promauto.NewCounter(prometheus.CounterOpts{
		Namespace: namespace,
		Name:      "scrape_error_count",
		Help:      "Number of scrape errors",
	})

	inverterSOCGauge = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "battery_soc",
		Help:      "state of charge of the BYD battery",
	}, []string{"battery"})

	inverterPowerGaugeVec = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "inverter_power",
		Help:      "Power flow of the inverter in Watt",
	}, []string{"inverter"})

	sitePowerLoadGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_load",
		Help:      "Site power load in Watt",
	})
	sitePowerGridGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_grid",
		Help:      "Site power supplied to or provided from the grid in Watt",
	})
	sitePowerAccuGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_accu",
		Help:      "Site power supplied to or provided from the accumulator(s) in Watt",
	})
	sitePowerPhotovoltaicsGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: namespace,
		Name:      "site_power_photovoltaic",
		Help:      "Site power supplied to or provided from the accumulator(s) in Watt",
	})

	batValueStateOfChargeRelativeGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "state_of_charge_relative",
		Help:      "battery state of charge relative",
	})
	batValueStateOfHealthRelativeGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "state_of_health_relative",
		Help:      "battery state of health relative",
	})
	batCapacityEstimationMaxGauge = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "capacity_estimation_max",
		Help:      "battery state of health relative",
	})
	batCapacityEstimationRemaining = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: batteryNamespace,
		Name:      "capacity_estimation_remaining",
		Help:      "capacity_estimation_remaining",
	})

	cloudinessYrFiveHours = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "cloudiness_yr_five_hours",
		Help:      "the cloudiness over the next 5 hours (yr.no)",
	})
	cloudinessFiveHours = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "cloudiness_five_hours",
		Help:      "the cloudiness over the next 5 hours",
	})
	targetSOC = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "target_soc",
		Help:      "the target soc",
	})
	optimisationActive = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "optimisation_actve",
		Help:      "shows the state of the optimisation",
	})
	currentTemp = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "current_temp",
		Help:      "current temperature",
	})
	currentHumidity = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "current_humidity",
		Help:      "current Humidity",
	})
	minTemperature24h = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "min_temperature_24h",
		Help:      "minimum temperature (24h)",
	})
	minTemperature24hYr = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "min_temperature_24h_yr",
		Help:      "minimum temperature (24h, yr)",
	})
	currentTempYr = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "current_temp_yr",
		Help:      "current temperature yr.no",
	})
	currentHumidityYr = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "current_humidity_yr",
		Help:      "current Humidity yr.no",
	})
	minTemperature48h = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "min_temperature_48h",
		Help:      "minimum temperature (48h)",
	})
	minTemperature48hYr = promauto.NewGauge(prometheus.GaugeOpts{
		Namespace: weatherNameSpace,
		Name:      "min_temperature_48h_yr",
		Help:      "minimum temperature (48h, yr)",
	})
	switchStateVec = promauto.NewGaugeVec(prometheus.GaugeOpts{
		Namespace: switchNameSpace,
		Name:      "current_state",
		Help:      "current state of the switch",
	}, []string{"tasmota"})
)

func collectMetricsFromTarget(clients fronius.Clients,
	config *cfg.Configuration) {
	start := time.Now()
	symoClient := clients.Symo
	batteryClient := clients.Battery
	log.WithFields(log.Fields{
		"url":     symoClient.Options.URL,
		"timeout": symoClient.Options.Timeout,
	}).Debug("Requesting data.")
	data, err := symoClient.GetPowerFlowData()
	if err != nil {
		log.WithError(err).Warn("Could not collect Symo metrics.")
		scrapeErrorCount.Add(1)
	} else {
		parseMetrics(data)
	}
	batteryData, err := batteryClient.GetBatteryData()
	if err != nil {
		log.WithError(err).Warn("Could not collect battery metrics.")
		scrapeErrorCount.Add(1)
	} else {
		parseBatteryMetrics(batteryData)
	}
	parseWeather(config, clients.Weather, clients.YrWeather)
	parseSwitches(clients.Switches)
	optimisationActive.Set(float64(fronius.State))
	elapsed := time.Since(start)
	scrapeDurationGauge.Set(elapsed.Seconds())
}

func parseSwitches(switches []*switchablesocket.SwitchClient) {
	for index, client := range switches {
		state, err := client.GetPowerState()
		if err != nil {
			log.Errorf("cannot create metric for switch %v: %s", index, err.Error())
		} else {
			switchStateVec.WithLabelValues(strconv.Itoa(index)).Set(float64(state))
		}
	}
}

func parseBatteryMetrics(data *fronius.BatteryData) {
	batValueStateOfChargeRelativeGauge.Set(data.Channels.BatValueStateOfChargeRelative)
	batValueStateOfHealthRelativeGauge.Set(data.Channels.BatValueStateOfHealthRelative)
	batCapacityEstimationMaxGauge.Set(data.Channels.BatCapacityEstimationMaxF64)
	batCapacityEstimationRemaining.Set(data.Channels.BatCapacityEstimationRemainingF64)
}

func parseMetrics(data *fronius.SymoData) {
	log.WithField("data", *data).Debug("Parsing data.")
	for key, inverter := range data.Inverters {
		inverterPowerGaugeVec.WithLabelValues(key).Set(inverter.Power)
		inverterSOCGauge.WithLabelValues(key).Set(inverter.SOC)
	}
	sitePowerAccuGauge.Set(data.Site.PowerAccu)
	sitePowerGridGauge.Set(data.Site.PowerGrid)
	sitePowerLoadGauge.Set(data.Site.PowerLoad)
	sitePowerPhotovoltaicsGauge.Set(data.Site.PowerPhotovoltaic)
}

func parseWeather(config *cfg.Configuration, weatherClient *weather.WeatherClient, weatherYrClient *weatherYr.WeatherClient) {
	weatherData := weatherClient.GetWeatherCached()
	cloudinessOverall := 500.
	if weatherData != nil {
		cloudinessOverall = weather.CheckCloudiness(weatherData, 5)
		cloudinessFiveHours.Set(cloudinessOverall)
		currentTemp.Set(weather.CurrentTemp(weatherData))
		currentHumidity.Set(float64(weather.CurrentHumidity(weatherData)))
		minTemperature24h.Set(weather.MinTemp(weatherData, 24))
		minTemperature48h.Set(weather.MinTemp(weatherData, 48))
	}
	weatherYrData := weatherYrClient.GetWeatherCached()
	if weatherYrData != nil {
		cloudinessYr := weatherYr.CheckCloudiness(weatherYrData, 5)
		cloudinessYrFiveHours.Set(cloudinessYr)
		cloudinessOverall = math.Max(cloudinessOverall, cloudinessYr)
		currentTempYr.Set(weatherYr.CurrentTemp(weatherYrData))
		currentHumidityYr.Set(weatherYr.CurrentHumidity(weatherYrData))
		minTemperature24hYr.Set(weatherYr.MinTemp(weatherYrData, 24))
		minTemperature48hYr.Set(weatherYr.MinTemp(weatherYrData, 48))
	}
	targetSOC.Set(fronius.GetTargetSocByCloud(config, cloudinessOverall))
}
