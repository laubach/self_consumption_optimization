package main

import (
	weatherYr "bitbucket.org/laubach/self_consumption_optimization/pkg/norwegian"
	switchablesocket "bitbucket.org/laubach/self_consumption_optimization/pkg/switch"
	"fmt"
	"github.com/bradfitz/gomemcache/memcache"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
	"os"
	"strconv"
	"time"

	"bitbucket.org/laubach/self_consumption_optimization/cfg"
	"bitbucket.org/laubach/self_consumption_optimization/pkg/fronius"
	weather "bitbucket.org/laubach/self_consumption_optimization/pkg/openweather"
	log "github.com/sirupsen/logrus"
	flag "github.com/spf13/pflag"
)

var (
	version     = "0.1"
	gitCommit   = "dirty"
	branch      = "any branch"
	date        = time.Now().String()
	config      = cfg.ParseConfig(version, gitCommit, date, flag.NewFlagSet("main", flag.ExitOnError), os.Args[1:])
	promHandler = promhttp.Handler()
)

func main() {

	log.WithFields(log.Fields{
		"version": version,
		"commit":  gitCommit,
		"date":    date,
		"branch":  branch,
	}).Info("Starting exporter.")

	headers := http.Header{}
	cfg.ConvertHeaders(config.Symo.Headers, &headers)

	var switches []*switchablesocket.SwitchClient

	for _, switchUrl := range config.Switch.URL {
		client, _ := switchablesocket.NewSwitchClient(switchablesocket.ClientOptions{
			URL:     switchUrl,
			Headers: headers,
			Timeout: config.Switch.Timeout,
		})
		switches = append(switches, client)
	}

	symoClient, _ := fronius.NewSymoClient(fronius.ClientOptions{
		URL:     config.Symo.URL,
		Headers: headers,
		Timeout: config.Symo.Timeout,
	})

	batteryClient, _ := fronius.NewBatteryClient(fronius.BatteryClientOptions{
		URL:     config.Symo.BatteryURL,
		Headers: headers,
		Timeout: config.Symo.Timeout,
	})

	weatherClient, _ := weather.NewWeatherClient(weather.WeatherClientOptions{
		URL:     config.Weather.URL,
		AppId:   config.Weather.AppId,
		Headers: headers,
		Timeout: config.Weather.Timeout,
	})
	weatherYrClient, _ := weatherYr.NewWeatherClient(weatherYr.WeatherClientOptions{
		URL:     config.Weather.YrUrl,
		Headers: headers,
		Timeout: config.Weather.Timeout,
	})

	clients := fronius.Clients{
		Battery:   batteryClient,
		Weather:   weatherClient,
		YrWeather: weatherYrClient,
		Symo:      symoClient,
		Switches:  switches,
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		if r.URL.Path != "/" {
			http.NotFound(w, r)
			return
		}
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed Root endpoint")
		http.Redirect(w, r, "/metrics", http.StatusMovedPermanently)
	})
	http.HandleFunc("/metrics", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed Metrics endpoint")
		collectMetricsFromTarget(clients, config)
		promHandler.ServeHTTP(w, r)
	})

	http.HandleFunc("/cache/weather", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed weather endpoint")
		cloudyHours := 5
		hours, ok := r.URL.Query()["hours"]
		if !ok || len(hours[0]) < 1 {
			log.Warnf("Url Param 'hours' is missing - using default %v", cloudyHours)
			log.Infof("endpoint has been called as: %v", r.URL.RequestURI())
		} else {
			ch, err := strconv.Atoi(hours[0])
			if err == nil {
				cloudyHours = ch
			} else {
				log.Warnf("param could not be read: %v", hours[0])
			}
		}
		// connect to memcache server
		mc := memcache.New("127.0.0.1:11211")

		// try to pull from memcache
		fetchItem, err := mc.Get(weather.CacheName)
		// check for cache hit
		if err != memcache.ErrCacheMiss {
			if err != nil {
				fmt.Println("Error fetching from memcache", err)
			} else {
				fmt.Println("Cache hit!")

				data, err := weather.DecodeData(fetchItem.Value)
				if err != nil {
					fmt.Println("Error decoding data from memcache", err)
				} else {
					fmt.Println(data)
					cloudiness := weather.CheckCloudiness(&data, cloudyHours)
					log.Infof("Cloudiness from main: %v", cloudiness)
					w.WriteHeader(http.StatusOK)
				}
			}
		}
	})

	http.HandleFunc("/weather", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed weather endpoint")
		cloudyHours := 5
		hours, ok := r.URL.Query()["hours"]
		if !ok || len(hours[0]) < 1 {
			log.Warnf("Url Param 'hours' is missing - using default %v", cloudyHours)
			log.Infof("endpoint has been called as: %v", r.URL.RequestURI())
		} else {
			ch, err := strconv.Atoi(hours[0])
			if err == nil {
				cloudyHours = ch
			} else {
				log.Warnf("param could not be read: %v", hours[0])
			}
		}
		data := weatherClient.GetWeatherCached()
		cloudiness := weather.CheckCloudiness(data, cloudyHours)
		log.Infof("Cloudiness from main: %v", cloudiness)
		w.WriteHeader(http.StatusOK)
	})

	http.HandleFunc("/switch", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed Digest auth endpoint")
		cmd, ok := r.URL.Query()["cmd"]
		if !ok || len(cmd[0]) < 1 {
			log.Println("Url Param 'cmd' is missing -> using default 'Power'")
			cmd = append(cmd, "Power")
		}

		for _, client := range switches {
			client.PowerState(cmd[0])
		}

		w.WriteHeader(http.StatusOK)
	})

	http.HandleFunc("/selfConsumption/automatic", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed Digest auth endpoint")

		resp := fronius.ChangeSelfConsumptionOption(0, 0, config)

		w.WriteHeader(resp.StatusCode)
	})

	http.HandleFunc("/selfConsumption/targetSoc", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed Digest auth endpoint")

		weatherData := weatherClient.GetWeatherCached()
		resp := fronius.ChangeSelfConsumptionOption(0, 0, config)
		now := time.Now()
		begin := time.Unix(weatherData.Current.Sunrise, 0)
		end := time.Unix(weatherData.Current.Sunset, 0).Add(-4 * time.Hour)
		log.Infof("now: %v", now)
		log.Infof("begin: %v", begin)
		log.Infof("end: %v", end)
		targetSoc := fronius.GetTargetSoc(now, begin, end, config)
		log.Infof("targetSoc: %v", targetSoc)
		targetSoc = fronius.GetTargetSocByCloud(config, weather.CheckCloudiness(weatherData, 5))
		log.Infof("targetSoc by cloudiness: %v", targetSoc)
		w.WriteHeader(resp.StatusCode)
	})

	http.HandleFunc("/ticker/start", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed Ticker endpoint")

		go fronius.StartOptimiseTicker(&clients, config)
		log.Info("started self consumption optimisation")

		w.WriteHeader(http.StatusOK)
	})
	http.HandleFunc("/ticker/stop", func(w http.ResponseWriter, r *http.Request) {
		log.WithFields(log.Fields{
			"uri":    r.RequestURI,
			"client": r.RemoteAddr,
		}).Debug("Accessed Ticker endpoint")

		log.Info("stopping self consumption optimisation")
		fronius.StopOptimiseTicker(config)
		log.Info("self consumption optimisation stopped")

		w.WriteHeader(http.StatusOK)
	})
	go fronius.StartWeatherTicker(&clients, config)
	log.WithField("port", config.BindAddr).Info("Listening for ticker start/stops.")
	log.WithError(http.ListenAndServe(config.BindAddr, nil)).Fatal("Shutting down.")
}
