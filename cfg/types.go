package cfg

import "time"

type (
	// Configuration holds a strongly-typed tree of the configuration
	Configuration struct {
		Log          LogConfig     `koanf:"log"`
		Symo         SymoConfig    `koanf:"symo"`
		Weather      WeatherConfig `koanf:"weather"`
		BindAddr     string        `koanf:"bind-addr"`
		Digest       Digest        `koanf:"digest"`
		Optimisation Optimisation  `koanf:"optimisation"`
		Switch       SwitchConfig  `koanf:"switch"`
	}
	// LogConfig configures the logging options
	LogConfig struct {
		Level   string `koanf:"level"`
		Verbose bool   `koanf:"verbose"`
	}
	// SymoConfig configures the Fronius Symo device
	SymoConfig struct {
		URL        string        `koanf:"url"`
		Timeout    time.Duration `koanf:"timeout"`
		Headers    []string      `koanf:"header"`
		BatteryURL string        `koanf:"batteryURL"`
	}

	SwitchConfig struct {
		URL     []string      `koanf:"url"`
		Timeout time.Duration `koanf:"timeout"`
		Headers []string      `koanf:"header"`
	}
	WeatherConfig struct {
		WaitTimeMinute int           `koanf:"waitMinute"`
		WaitTimeHour   int           `koanf:"waitHour"`
		URL            string        `koanf:"url"`
		YrUrl          string        `koanf:"yrurl"`
		Timeout        time.Duration `koanf:"timeout"`
		Headers        []string      `koanf:"header"`
		AppId          string        `koanf:"appid"`
		CloudLimit     int           `koanf:"clouds"`
	}
	Digest struct {
		User     string `koanf:"user"`
		Password string `koanf:"password"`
	}
	Optimisation struct {
		WaitTime     int     `koanf:"wait"`
		SocStart     float64 `koanf:"soc"`
		SocEnd       float64 `koanf:"socEnd"`
		BeforeSunset int     `koanf:"beforeSunset"`
		StartAt      int     `koanf:"startAt"`
		EndAt        int     `koanf:"endAt"`
	}
)

// NewDefaultConfig retrieves the hardcoded configs with sane defaults
func NewDefaultConfig() *Configuration {
	return &Configuration{
		Log: LogConfig{
			Level: "info",
		},
		Switch: SwitchConfig{
			URL:     []string{"http://tasmota_1/cm", "http://tasmota_2/cm", "http://tasmota_3/cm"},
			Timeout: 5 * time.Second,
			Headers: []string{},
		},
		Symo: SymoConfig{
			URL:        "http://gen24/solar_api/v1/GetPowerFlowRealtimeData.fcgi",
			BatteryURL: "http://gen24/components/16580608/readable",
			Timeout:    5 * time.Second,
			Headers:    []string{},
		},
		Digest: Digest{
			User:     "customer",
			Password: "secret",
		},
		Optimisation: Optimisation{
			WaitTime:     60,
			SocStart:     80.,
			SocEnd:       90.,
			EndAt:        14,
			StartAt:      8,
			BeforeSunset: 2,
		},
		Weather: WeatherConfig{
			WaitTimeMinute: 10,
			WaitTimeHour:   0,
			URL:            "https://api.openweathermap.org/data/2.5/onecall?lat=48.3849&lon=10.7948&exclude=&units=metric",
			YrUrl:          "https://api.met.no/weatherapi/locationforecast/2.0/compact?lat=48.3849&lon=10.7948",
			Timeout:        5 * time.Second,
			Headers:        []string{},
			CloudLimit:     180,
		},
		BindAddr: ":9202",
	}
}
