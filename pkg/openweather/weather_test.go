package openweather

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_WeatherData_GivenUrl_WhenRequestData_ThenParseStruct(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		payload, err := ioutil.ReadFile("testdata/weather.json")
		require.NoError(t, err)
		rw.Write(payload)
	}))

	c, err := NewWeatherClient(WeatherClientOptions{
		URL: server.URL,
	})
	require.NoError(t, err)

	p, err := c.getWeatherData()
	assert.NoError(t, err)
	assert.Equal(t, 40, p.Current.Clouds)
	assert.Equal(t, 1627843930, p.Current.Dt)
	assert.Equal(t, int64(1627789904), p.Current.Sunrise)
	assert.Equal(t, int64(1627844066), p.Current.Sunset)
	assert.Equal(t, 14.47, p.Current.Temp)
	assert.Equal(t, 14.3, p.Current.FeelsLike)
	assert.Equal(t, 1012, p.Current.Pressure)
	assert.Equal(t, 89, p.Current.Humidity)
	assert.Equal(t, 12.68, p.Current.DewPoint)
	assert.Equal(t, 0., p.Current.Uvi)
	assert.Equal(t, 40, p.Current.Clouds)
	assert.Equal(t, 10000, p.Current.Visibility)
	assert.Equal(t, 4.12, p.Current.WindSpeed)
	assert.Equal(t, 250, p.Current.WindDeg)
	assert.Equal(t, 2.37, p.Current.Rain.OneH)
	assert.Equal(t, 284., CheckCloudiness(p, 5))
	assert.Equal(t, 10.14, MinTemp(p, 48))
	assert.Equal(t, 14.47, CurrentTemp(p))
	assert.Equal(t, 62, CurrentHumidity(p))
}
