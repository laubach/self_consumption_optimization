package openweather

import (
	"encoding/json"
	"fmt"
	"github.com/bradfitz/gomemcache/memcache"
	log "github.com/sirupsen/logrus"
	"math"
	"net/http"
	"net/url"
	"time"
)

const (
	CacheName = "openWeather"
)

type (
	WeatherData struct {
		Current struct {
			Clouds    int     `json:"clouds"`
			DewPoint  float64 `json:"dew_point"`
			Dt        int     `json:"dt"`
			FeelsLike float64 `json:"feels_like"`
			Humidity  int     `json:"humidity"`
			Pressure  int     `json:"pressure"`
			Rain      struct {
				OneH float64 `json:"1h"`
			} `json:"rain"`
			Sunrise    int64   `json:"sunrise"`
			Sunset     int64   `json:"sunset"`
			Temp       float64 `json:"temp"`
			Uvi        float64 `json:"uvi"`
			Visibility int     `json:"visibility"`
			Weather    []struct {
				Description string `json:"description"`
				Icon        string `json:"icon"`
				ID          int    `json:"id"`
				Main        string `json:"main"`
			} `json:"weather"`
			WindDeg   int     `json:"wind_deg"`
			WindSpeed float64 `json:"wind_speed"`
		} `json:"current"`
		Daily []struct {
			Clouds    int     `json:"clouds"`
			DewPoint  float64 `json:"dew_point"`
			Dt        int     `json:"dt"`
			FeelsLike struct {
				Day   float64 `json:"day"`
				Eve   float64 `json:"eve"`
				Morn  float64 `json:"morn"`
				Night float64 `json:"night"`
			} `json:"feels_like"`
			Humidity  int     `json:"humidity"`
			MoonPhase float64 `json:"moon_phase"`
			Moonrise  int     `json:"moonrise"`
			Moonset   int     `json:"moonset"`
			Pop       float64 `json:"pop"`
			Pressure  int     `json:"pressure"`
			Rain      float64 `json:"rain,omitempty"`
			Sunrise   int     `json:"sunrise"`
			Sunset    int     `json:"sunset"`
			Temp      struct {
				Day   float64 `json:"day"`
				Eve   float64 `json:"eve"`
				Max   float64 `json:"max"`
				Min   float64 `json:"min"`
				Morn  float64 `json:"morn"`
				Night float64 `json:"night"`
			} `json:"temp"`
			Uvi     float64 `json:"uvi"`
			Weather []struct {
				Description string `json:"description"`
				Icon        string `json:"icon"`
				ID          int    `json:"id"`
				Main        string `json:"main"`
			} `json:"weather"`
			WindDeg   int     `json:"wind_deg"`
			WindGust  float64 `json:"wind_gust"`
			WindSpeed float64 `json:"wind_speed"`
		} `json:"daily"`
		Hourly []struct {
			Clouds    int     `json:"clouds"`
			DewPoint  float64 `json:"dew_point"`
			Dt        int     `json:"dt"`
			FeelsLike float64 `json:"feels_like"`
			Humidity  int     `json:"humidity"`
			Pop       float64 `json:"pop"`
			Pressure  int     `json:"pressure"`
			Rain      struct {
				OneH float64 `json:"1h"`
			} `json:"rain,omitempty"`
			Temp       float64 `json:"temp"`
			Uvi        float64 `json:"uvi"`
			Visibility int     `json:"visibility"`
			Weather    []struct {
				Description string `json:"description"`
				Icon        string `json:"icon"`
				ID          int    `json:"id"`
				Main        string `json:"main"`
			} `json:"weather"`
			WindDeg   int     `json:"wind_deg"`
			WindGust  float64 `json:"wind_gust"`
			WindSpeed float64 `json:"wind_speed"`
		} `json:"hourly"`
		Lat      float64 `json:"lat"`
		Lon      float64 `json:"lon"`
		Minutely []struct {
			Dt            int     `json:"dt"`
			Precipitation float64 `json:"precipitation"`
		} `json:"minutely"`
		Timezone       string `json:"timezone"`
		TimezoneOffset int    `json:"timezone_offset"`
	}
	// WeatherClient is a wrapper for making API requests against a Fronius Fronius Gen24 Wechselrichter Battery.
	WeatherClient struct {
		request *http.Request
		Options WeatherClientOptions
	}
	// WeatherClientOptions holds some parameters for the WeatherClient.
	WeatherClientOptions struct {
		URL     string
		AppId   string
		Headers http.Header
		Timeout time.Duration
	}
)

// NewWeatherClient constructs a WeatherClient ready to use for collecting weather data.
func NewWeatherClient(options WeatherClientOptions) (*WeatherClient, error) {
	u, err := url.Parse(options.URL)
	if err != nil {
		return nil, err
	}
	request := http.Request{
		URL:    u,
		Header: options.Headers,
	}
	q := request.URL.Query()
	q.Add("appid", options.AppId)
	request.URL.RawQuery = q.Encode()
	return &WeatherClient{
		request: &request,
		Options: options,
	}, nil
}

// getWeatherData returns the parsed data from the Gen24 Meter device.
func (c *WeatherClient) getWeatherData() (*WeatherData, error) {
	client := http.DefaultClient
	client.Timeout = c.Options.Timeout
	response, err := client.Do(c.request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	p := WeatherData{}
	err = json.NewDecoder(response.Body).Decode(&p)
	if err != nil {
		return nil, err
	}

	mc := memcache.New("127.0.0.1:11211")
	setItem := memcache.Item{
		Key:        CacheName,
		Value:      EncodeData(p),
		Expiration: 600,
	}
	err = mc.Set(&setItem)
	if err != nil {
		fmt.Println("Error setting memcache item", err)
	}
	log.Infof("succesfully fetched weather data via %v", CacheName)
	return &p, nil
}

func DecodeData(raw []byte) (data WeatherData, err error) {
	err = json.Unmarshal(raw, &data)
	return data, err
}

func EncodeData(data WeatherData) []byte {
	enc, err := json.Marshal(data)
	if err != nil {
		fmt.Println("Error encoding Action to JSON", err)
	}
	return enc
}

func CheckCloudiness(data *WeatherData, hours int) float64 {
	cloudy := 0
	for i := 0; i < hours; i++ {
		log.Debugf("%v: CLOUDY: %v", time.Unix(int64(data.Hourly[i].Dt), 0), data.Hourly[i].Clouds)
		cloudy += data.Hourly[i].Clouds
	}
	log.Debugf("Cloudiness of the next %v hours (%v): %v", hours, CacheName, cloudy)
	return float64(cloudy)
}

func MinTemp(data *WeatherData, hours int) float64 {
	minTemp := 100.
	for i := 0; i < hours; i++ {
		minTemp = math.Min(minTemp, data.Hourly[i].Temp)
	}
	log.Debugf("Minimum temperature in the next %v hours (%v): %v", hours, CacheName, minTemp)
	return minTemp
}

func CurrentTemp(data *WeatherData) float64 {
	currentTemp := data.Current.Temp
	log.Debugf("Current temperature: %v", currentTemp)
	return currentTemp
}

func CurrentHumidity(data *WeatherData) int {
	relHumidity := data.Current.Humidity
	log.Debugf("Current relative humidity: %v", relHumidity)
	return relHumidity
}

func (c *WeatherClient) GetWeatherCached() *WeatherData {

	// connect to memcache server
	mc := memcache.New("127.0.0.1:11211")

	// try to pull from memcache
	fetchItem, err := mc.Get(CacheName)
	// check for cache hit
	if err != memcache.ErrCacheMiss {
		if err != nil {
			log.Errorf("Error fetching from memcache: %v", err)
		} else {
			log.Debugf("Cache hit for %v!", CacheName)
			log.Debug(string(fetchItem.Value))
			data, err := DecodeData(fetchItem.Value)
			if err != nil {
				log.Errorf("Error decoding data from memcache: %v", err)
			} else {
				return &data
			}
		}
	} else {
		data, clientError := c.getWeatherData()
		if clientError == nil {
			return data
		} else {
			log.Errorf("Error fetching data from openweathermap: %v", clientError)
		}
	}
	return nil
}
