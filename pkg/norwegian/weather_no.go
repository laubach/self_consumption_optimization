package yr_no

import (
	"encoding/json"
	"fmt"
	"github.com/bradfitz/gomemcache/memcache"
	log "github.com/sirupsen/logrus"
	"math"
	"net/http"
	"net/url"
	"time"
)

const (
	CacheName = "weather_yr_no"
)

type (
	WeatherData struct {
		Type     string `json:"type"`
		Geometry struct {
			Type        string    `json:"type"`
			Coordinates []float32 `json:"coordinates"`
		} `json:"geometry"`
		Properties struct {
			Meta struct {
				UpdatedAt time.Time `json:"updated_at"`
				Units     struct {
					AirPressureAtSeaLevel string `json:"air_pressure_at_sea_level"`
					AirTemperature        string `json:"air_temperature"`
					CloudAreaFraction     string `json:"cloud_area_fraction"`
					PrecipitationAmount   string `json:"precipitation_amount"`
					RelativeHumidity      string `json:"relative_humidity"`
					WindFromDirection     string `json:"wind_from_direction"`
					WindSpeed             string `json:"wind_speed"`
				} `json:"units"`
			} `json:"meta"`
			TimeSeries []struct {
				Time time.Time `json:"time"`
				Data struct {
					Instant struct {
						Details struct {
							AirPressureAtSeaLevel float64 `json:"air_pressure_at_sea_level"`
							AirTemperature        float64 `json:"air_temperature"`
							CloudAreaFraction     float64 `json:"cloud_area_fraction"`
							RelativeHumidity      float64 `json:"relative_humidity"`
							WindFromDirection     float64 `json:"wind_from_direction"`
							WindSpeed             float64 `json:"wind_speed"`
						} `json:"details"`
					} `json:"instant"`
					Next12Hours struct {
						Summary struct {
							SymbolCode string `json:"symbol_code"`
						} `json:"summary"`
					} `json:"next_12_hours"`
					Next1Hours struct {
						Summary struct {
							SymbolCode string `json:"symbol_code"`
						} `json:"summary"`
						Details struct {
							PrecipitationAmount float64 `json:"precipitation_amount"`
						} `json:"details"`
					} `json:"next_1_hours"`
					Next6Hours struct {
						Summary struct {
							SymbolCode string `json:"symbol_code"`
						} `json:"summary"`
						Details struct {
							PrecipitationAmount float64 `json:"precipitation_amount"`
						} `json:"details"`
					} `json:"next_6_hours"`
				} `json:"data"`
			} `json:"timeseries"`
		} `json:"properties"`
	}
	// WeatherClient is a wrapper for making API requests against a Fronius Fronius Gen24 Wechselrichter Battery.
	WeatherClient struct {
		request *http.Request
		Options WeatherClientOptions
	}
	// WeatherClientOptions holds some parameters for the WeatherClient.
	WeatherClientOptions struct {
		URL     string
		Headers http.Header
		Timeout time.Duration
	}
)

// NewWeatherClient constructs a WeatherClient ready to use for collecting weather data.
func NewWeatherClient(options WeatherClientOptions) (*WeatherClient, error) {
	u, err := url.Parse(options.URL)
	if err != nil {
		return nil, err
	}
	options.Headers = http.Header{}
	log.Debug("headers ...")
	log.Debugf("headers available: %s", options.Headers)
	options.Headers.Add("User-Agent", "Golang_Spider_Bot/3.0")
	request := http.Request{
		URL:    u,
		Header: options.Headers,
	}
	q := request.URL.Query()
	request.URL.RawQuery = q.Encode()
	return &WeatherClient{
		request: &request,
		Options: options,
	}, nil
}

// getWeatherData returns the parsed data from the Gen24 Meter device.
func (c *WeatherClient) getWeatherData() (*WeatherData, error) {
	client := http.DefaultClient
	client.Timeout = c.Options.Timeout
	response, err := client.Do(c.request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	p := WeatherData{}
	err = json.NewDecoder(response.Body).Decode(&p)
	if err != nil {
		return nil, err
	}

	mc := memcache.New("127.0.0.1:11211")
	setItem := memcache.Item{
		Key:        CacheName,
		Value:      EncodeData(p),
		Expiration: 600,
	}
	err = mc.Set(&setItem)
	if err != nil {
		fmt.Println("Error setting memcache item", err)
	}
	log.Debugf("succesfully fetched weather data via %v", CacheName)
	return &p, nil
}

func DecodeData(raw []byte) (data WeatherData, err error) {
	err = json.Unmarshal(raw, &data)
	return data, err
}

func EncodeData(data WeatherData) []byte {
	enc, err := json.Marshal(data)
	if err != nil {
		fmt.Println("Error encoding Action to JSON", err)
	}
	return enc
}

func MinTemp(data *WeatherData, hours int) float64 {
	minTemp := 100.
	for i := 0; i < hours; i++ {
		minTemp = math.Min(minTemp, data.Properties.TimeSeries[i].Data.Instant.Details.AirTemperature)
	}
	log.Debugf("Minimum temperature in the next %v hours (%v): %v", hours, CacheName, minTemp)
	return minTemp
}

func CurrentTemp(data *WeatherData) float64 {
	currentTemp := data.Properties.TimeSeries[0].Data.Instant.Details.AirTemperature
	log.Debugf("Current temperature: %v", currentTemp)
	return currentTemp
}

func CurrentHumidity(data *WeatherData) float64 {
	relHumidity := data.Properties.TimeSeries[0].Data.Instant.Details.RelativeHumidity
	log.Debugf("Current relative humidity: %v", relHumidity)
	return relHumidity
}

func CheckCloudiness(data *WeatherData, hours int) float64 {
	cloudy := 0.
	for i := 0; i < hours; i++ {
		cloudy += data.Properties.TimeSeries[i].Data.Instant.Details.CloudAreaFraction
	}
	log.Debugf("Cloudiness of the next %v hours (%v): %v", hours, CacheName, cloudy)
	return cloudy
}

func (c *WeatherClient) GetWeatherCached() *WeatherData {

	// connect to memcache server
	mc := memcache.New("127.0.0.1:11211")

	// try to pull from memcache
	fetchItem, err := mc.Get(CacheName)
	// check for cache hit
	if err != memcache.ErrCacheMiss {
		if err != nil {
			log.Errorf("Error fetching from memcache: %v", err)
		} else {
			log.Debugf("Cache hit for %v!", CacheName)
			log.Debug(string(fetchItem.Value))
			data, err := DecodeData(fetchItem.Value)
			if err != nil {
				log.Errorf("Error decoding data from memcache: %v", err)
			} else {
				return &data
			}
		}
	} else {
		data, clientError := c.getWeatherData()
		if clientError == nil {
			return data
		} else {
			log.Errorf("Error fetching data from yr.no: %v", clientError)
		}
	}
	return nil
}
