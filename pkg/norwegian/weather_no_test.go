package yr_no

import (
	log "github.com/sirupsen/logrus"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_WeatherData_GivenUrl_WhenRequestData_ThenParseStruct(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		payload, err := ioutil.ReadFile("testdata/yr_no_testdata.json")
		require.NoError(t, err)
		write, err := rw.Write(payload)
		log.Debug(write)
		if err != nil {
			log.Error(err)
			return
		}
	}))

	c, err := NewWeatherClient(WeatherClientOptions{
		URL: server.URL,
	})
	require.NoError(t, err)

	p, err := c.getWeatherData()
	assert.NoError(t, err)
	details0 := p.Properties.TimeSeries[0].Data.Instant.Details
	assert.Equal(t, 1017.0, details0.AirPressureAtSeaLevel)
	assert.Equal(t, 21.7, details0.AirTemperature)
	assert.Equal(t, 0.0, details0.CloudAreaFraction)
	assert.Equal(t, 66.3, details0.RelativeHumidity)
	assert.Equal(t, 183.3, details0.WindFromDirection)
	assert.Equal(t, 0.0, details0.WindSpeed)
	assert.Equal(t, 1.6+40.6+20.3+68.7, CheckCloudiness(p, 6))
	assert.Equal(t, 18.9, MinTemp(p, 24))
	assert.Equal(t, 12.4, MinTemp(p, 48))
	assert.Equal(t, 21.7, CurrentTemp(p))
	assert.Equal(t, 66.3, CurrentHumidity(p))
}
