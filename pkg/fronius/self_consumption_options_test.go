package fronius

import (
	"bitbucket.org/laubach/self_consumption_optimization/cfg"
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

var (
	config = cfg.DefaultConfig()
)

func Test_getLimits(t *testing.T) {
	ghostHour := time.Now().Truncate(24 * time.Hour)
	targetSoc := getter(0)
	assert.Equal(t, targetSoc, 60.)
	assert.Equal(t, ghostHour.Hour(), 2)
	assert.Equal(t, ghostHour.UTC().Add(6*time.Hour).Hour(), 6)
	assert.Equal(t, time.Now().Truncate(24*time.Hour).Add(8*time.Hour).UTC().Hour(), 8)
	assert.Equal(t, 80., getter(10))
	assert.Equal(t, 84., getter(12))
	assert.Equal(t, 90., getter(15))
	assert.Equal(t, 94., getter(17))
	assert.Equal(t, 100., getter(20))
}

func getter(hours int) float64 {
	now := time.Now()
	appointment := now.Truncate(24 * time.Hour).Add(time.Duration(hours) * time.Hour)
	begin := now.Truncate(24 * time.Hour).Add(time.Duration(10) * time.Hour)
	end := now.Truncate(24 * time.Hour).Add(time.Duration(20) * time.Hour)
	return GetTargetSoc(appointment, begin, end, config)
}
func Test_GetTargetSocByCloud(t *testing.T) {
	log.Infof("socmin %v. socmax = %v", config.Optimisation.SocStart, config.Optimisation.SocEnd)
	for i := 0; i <= 500; i++ {
		socByCloud := GetTargetSocByCloud(config, float64(i-100))
		log.Infof("iteration %v. SOC = %v", i, socByCloud)
		assert.LessOrEqual(t, socByCloud, config.Optimisation.SocEnd)
		assert.GreaterOrEqual(t, socByCloud, config.Optimisation.SocStart)
	}
}
