package fronius

import (
	"encoding/json"
	"net/http"
	"net/url"
	"time"
)

type (
	batteryFlow struct {
		Body struct {
			Data BatteryData
		}
	}
	// BatteryData holds the parsed data from the Symo API.
	BatteryData struct {
		Channels BatteryChannels `json:"channels"`
	}
	BatteryChannels struct {
		BatCapacityEstimationMaxF64          float64 `json:"BAT_CAPACITY_ESTIMATION_MAX_F64"`
		BatCapacityEstimationRemainingF64    float64 `json:"BAT_CAPACITY_ESTIMATION_REMAINING_F64"`
		BatCurrentDcF64                      float64 `json:"BAT_CURRENT_DC_F64"`
		BatCurrentDcInternalF64              float64 `json:"BAT_CURRENT_DC_INTERNAL_F64"`
		BatEnergyactiveLifetimeChargedF64    float64 `json:"BAT_ENERGYACTIVE_LIFETIME_CHARGED_F64"`
		BatEnergyactiveLifetimeDischargedF64 float64 `json:"BAT_ENERGYACTIVE_LIFETIME_DISCHARGED_F64"`
		BatModeCellStateU16                  float64 `json:"BAT_MODE_CELL_STATE_U16"`
		BatModeHybridOperatingStateU16       float64 `json:"BAT_MODE_HYBRID_OPERATING_STATE_U16"`
		BatModeStateU16                      float64 `json:"BAT_MODE_STATE_U16"`
		BatModeU16                           float64 `json:"BAT_MODE_U16"`
		BatModeWakeEnableStatusU16           float64 `json:"BAT_MODE_WAKE_ENABLE_STATUS_U16"`
		BatTemperatureCellF64                float64 `json:"BAT_TEMPERATURE_CELL_F64"`
		BatTemperatureCellMaxF64             float64 `json:"BAT_TEMPERATURE_CELL_MAX_F64"`
		BatTemperatureCellMinF64             float64 `json:"BAT_TEMPERATURE_CELL_MIN_F64"`
		BatValueStateOfChargeRelative        float64 `json:"BAT_VALUE_STATE_OF_CHARGE_RELATIVE_U16"`
		BatValueStateOfHealthRelative        float64 `json:"BAT_VALUE_STATE_OF_HEALTH_RELATIVE_U16"`
		BatVoltageDcInternalF64              float64 `json:"BAT_VOLTAGE_DC_INTERNAL_F64"`
		ComponentsModeEnableU16              float64 `json:"COMPONENTS_MODE_ENABLE_U16"`
		ComponentsModeVisibleU16             float64 `json:"COMPONENTS_MODE_VISIBLE_U16"`
		ComponentsTimeStampU64               float64 `json:"COMPONENTS_TIME_STAMP_U64"`
		DclinkPoweractiveLimitDischargeF64   float64 `json:"DCLINK_POWERACTIVE_LIMIT_DISCHARGE_F64"`
		DclinkPoweractiveMaxF32              float64 `json:"DCLINK_POWERACTIVE_MAX_F32"`
		DclinkVoltageMeanF32                 float64 `json:"DCLINK_VOLTAGE_MEAN_F32"`
		DeviceTemperatureAmbientemeanF32     float64 `json:"DEVICE_TEMPERATURE_AMBIENTEMEAN_F32"`
	}
	// BatteryClient is a wrapper for making API requests against a Fronius Fronius Gen24 Wechselrichter Battery.
	BatteryClient struct {
		request *http.Request
		Options BatteryClientOptions
	}
	// BatteryClientOptions holds some parameters for the BatteryClient.
	BatteryClientOptions struct {
		URL     string
		Headers http.Header
		Timeout time.Duration
	}
)

// NewBatteryClient constructs a BatteryClient ready to use for collecting metrics.
func NewBatteryClient(options BatteryClientOptions) (*BatteryClient, error) {
	u, err := url.Parse(options.URL)
	if err != nil {
		return nil, err
	}
	return &BatteryClient{
		request: &http.Request{
			URL:    u,
			Header: options.Headers,
		},
		Options: options,
	}, nil
}

// GetBatteryData returns the parsed data from the Gen24 Meter device.
func (c *BatteryClient) GetBatteryData() (*BatteryData, error) {
	client := http.DefaultClient
	client.Timeout = c.Options.Timeout
	response, err := client.Do(c.request)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	p := batteryFlow{}
	err = json.NewDecoder(response.Body).Decode(&p)
	if err != nil {
		return nil, err
	}
	return &p.Body.Data, nil
}
