package fronius

import (
	"bitbucket.org/laubach/self_consumption_optimization/cfg"
	"bitbucket.org/laubach/self_consumption_optimization/pkg/digest"
	weatherYr "bitbucket.org/laubach/self_consumption_optimization/pkg/norwegian"
	weather "bitbucket.org/laubach/self_consumption_optimization/pkg/openweather"
	switchablesocket "bitbucket.org/laubach/self_consumption_optimization/pkg/switch"
	"encoding/json"
	"fmt"
	log "github.com/sirupsen/logrus"
	"math"
	"net/http"
	"strings"
	"time"
)

var (
	optimiseTicker = time.NewTicker(time.Minute)
	weatherTicker  = time.NewTicker(time.Hour)
	switchTicker   = time.NewTicker(time.Minute)
	State          = 0
)

type Clients struct {
	Battery   *BatteryClient
	Weather   *weather.WeatherClient
	YrWeather *weatherYr.WeatherClient
	Symo      *SymoClient
	Switches  []*switchablesocket.SwitchClient
}

func ChangeSelfConsumptionOption(power int, enabled int, configuration *cfg.Configuration) *http.Response {
	username := configuration.Digest.User
	password := configuration.Digest.Password
	uri := "http://gen24/config/batteries"
	t := digest.NewTransport(username, password)

	req := getRequest(http.MethodGet, uri, "")
	resp := execRequestWithDigestRoundtrip(t, req)
	defer resp.Body.Close()
	if http.StatusOK == resp.StatusCode {
		p := batteryConfig{}
		err := json.NewDecoder(resp.Body).Decode(&p)
		if err != nil {
			log.Errorf("decoding failed after get config: %v", err)
		}
		log.Debugf("battery configuration currently: %v", p)
		if p.HybEmPower != power || p.HybEmMode != enabled {
			payload := fmt.Sprintf("{\"HYB_EM_POWER\":-%d,\"HYB_EM_MODE\":%d}", power, enabled)
			reqPost := getRequest(http.MethodPost, uri, payload)
			respPost := execRequestWithDigestRoundtrip(t, reqPost)
			defer respPost.Body.Close()
			if respPost.StatusCode == http.StatusOK {
				log.Infof("successfully applied new options to:\n\tstatus: %v\n\tpower: %v", enabled, power)
			} else {
				log.Warn("update of self consumption options failed.")
			}
		} else {
			log.Debug("configuration already in correct state, nothing to do.")
		}
	} else {
		log.Warnf("could not get current state of self consumption options: %v ", resp.StatusCode)
	}
	return resp
}

func getRequest(methodGet string, uri string, payload string) *http.Request {
	req, err := http.NewRequest(methodGet, uri, strings.NewReader(payload))
	if err != nil {
		log.Fatalln(err)
	}
	return req
}

func execRequestWithDigestRoundtrip(t *digest.Transport, req *http.Request) *http.Response {
	resp, err := t.RoundTrip(req)
	if err != nil {
		log.Fatalln(err)
	}

	log.Debugf("%v %v", resp.Proto, resp.Status)
	for name, values := range resp.Header {
		// Loop over all values for the name.
		for _, value := range values {
			log.Debugf("header | %q : %q", name, value)
		}
	}
	return resp
}
func StartOptimiseTicker(clients *Clients, configuration *cfg.Configuration) {
	duration := time.Duration(configuration.Optimisation.WaitTime) * time.Second
	log.Infof("starting self consumption optimisation (evaluation every %v minutes)", duration.Minutes())
	optimise(clients, configuration)
	State = 1
	optimiseTicker.Reset(duration)
	for {
		select {
		case t := <-optimiseTicker.C:
			log.Debug("Current time (optimiseTicker.C): ", t)
			optimise(clients, configuration)
		}
	}
}
func StartSwitchTicker(clients *Clients) {
	duration := time.Duration(60) * time.Second
	log.Infof("power saving via switch (evaluation every %v minutes)", duration.Minutes())
	optimiseSwitch(clients)
	State = 1
	switchTicker.Reset(duration)
	for {
		select {
		case t := <-switchTicker.C:
			log.Debug("Current time (switchTicker.C): ", t)
			optimiseSwitch(clients)
		}
	}
}
func StartWeatherTicker(clients *Clients, configuration *cfg.Configuration) {
	duration := time.Duration(configuration.Weather.WaitTimeMinute)*time.Minute + time.Duration(configuration.Weather.WaitTimeHour)*time.Hour
	log.Infof("checking weather every %v minutes)", duration.Minutes())
	startStopOptimisationOnCloudiness(clients, configuration, time.Now())
	weatherTicker.Reset(duration)
	for {
		select {
		case t := <-weatherTicker.C:
			startStopOptimisationOnCloudiness(clients, configuration, t)
		}
	}
}

func startStopOptimisationOnCloudiness(clients *Clients, configuration *cfg.Configuration, t time.Time) {
	log.Debug("Current time (startStopOptimisationOnCloudiness): ", t)
	openWeatherData := clients.Weather.GetWeatherCached()
	if openWeatherData == nil {
		log.Warn("failed to fetch weather -> disable optimisation")
		go StopOptimiseTicker(configuration)
	} else {
		now := time.Now()
		begin := time.Unix(openWeatherData.Current.Sunrise, 0).UTC()
		sunset := time.Unix(openWeatherData.Current.Sunset, 0).UTC()
		end := sunset.Add(-time.Duration(configuration.Optimisation.BeforeSunset) * time.Hour)
		if now.After(begin) && now.Before(end) {
			go StartOptimiseTicker(clients, configuration)
		} else {
			log.Info("stopping optimisation during night (3hours befor sunset)")
			go StopOptimiseTicker(configuration)
		}
		if now.After(begin) && now.Before(sunset) {
			go StartSwitchTicker(clients)
		} else {
			log.Debug("deactivate switches during night")
			go StopSwitchTicker(clients)
		}
	}
}

func StopOptimiseTicker(configuration *cfg.Configuration) {
	State = 0
	defer optimiseTicker.Stop()
	log.Debug("Stopping automatic optimisation -> setting self consumption option to automatic")
	ChangeSelfConsumptionOption(0, 0, configuration)
}
func StopSwitchTicker(clients *Clients) {
	State = 0
	defer switchTicker.Stop()
	log.Debug("Stopping optimisation by switching all OFF")
	executeAllSwitches(clients, switchablesocket.CmdOff)
}

func executeAllSwitches(clients *Clients, command string) {
	for _, client := range clients.Switches {
		client.PowerState(command)
	}
}
func GetAllSwitchesState(clients *Clients) bool {
	states := 0
	for _, client := range clients.Switches {
		state, err := client.GetPowerState()
		if err != nil {
			return false
		}
		states += int(state)
	}
	result := states == len(clients.Switches)
	log.Infof("all clients state: %v", result)
	return result
}

func optimise(clients *Clients, configuration *cfg.Configuration) {
	batteryClient := clients.Battery
	log.WithFields(log.Fields{
		"url":     batteryClient.Options.URL,
		"timeout": batteryClient.Options.Timeout,
	}).Debug("Requesting battery data.")
	batteryData, errBattery := batteryClient.GetBatteryData()
	if errBattery != nil {
		log.WithError(errBattery).Warn("Could not collect state of charge for the battery.")
	} else {
		weatherData := clients.Weather.GetWeatherCached()
		yrWeatherData := clients.YrWeather.GetWeatherCached()
		cloudiness := 500.
		if weatherData != nil {
			cloudiness = weather.CheckCloudiness(weatherData, 5)
		}
		if yrWeatherData != nil {
			cloudiness = math.Max(cloudiness, weatherYr.CheckCloudiness(yrWeatherData, 5))
		}
		targetSoc := GetTargetSocByCloud(configuration, cloudiness)
		symoData, errorSymo := clients.Symo.GetPowerFlowData()
		if errorSymo != nil {
			log.WithError(errorSymo).Warn("Could not collect inverter data.")
		} else {
			currentSoc := batteryData.Channels.BatValueStateOfChargeRelative
			photovoltaic := symoData.Site.PowerPhotovoltaic
			load := symoData.Site.PowerLoad
			feedInPower := math.Copysign(photovoltaic+load+100*(currentSoc-targetSoc)*(currentSoc-targetSoc), currentSoc-targetSoc)
			feedInPower = math.Max(0, feedInPower)
			feedInPower = math.Min(10000, feedInPower)
			log.Infof("setting self consumption option to manual configuration. Current SOC: %0.2f Target SOC: %0.2f", currentSoc, targetSoc)
			log.Infof("Current PV power - consumption: %0.1fW - %0.1fW. Target at Feed-In Point: %vW", photovoltaic, load, feedInPower)
			ChangeSelfConsumptionOption(int(feedInPower), 1, configuration)
		}
	}
}

func optimiseSwitch(clients *Clients) {
	batteryClient := clients.Battery
	log.WithFields(log.Fields{
		"url":     batteryClient.Options.URL,
		"timeout": batteryClient.Options.Timeout,
	}).Debug("Requesting battery data.")
	batteryData, errBattery := batteryClient.GetBatteryData()
	if errBattery != nil {
		log.WithError(errBattery).Warn("Could not collect state of charge for the battery.")
	} else {
		symoData, _ := clients.Symo.GetPowerFlowData()
		currentSoc := batteryData.Channels.BatValueStateOfChargeRelative
		/* depending on the current switch states I should have different decissions:
		   * currently ON:
				* if isGainingEnergy(,0) switch off
			* currently OFF:
				* if isGainingEnergy(,100) switch on
		*/
		if switchCondition(clients, symoData, currentSoc) {
			executeAllSwitches(clients, switchablesocket.CmdOn)
		} else {
			executeAllSwitches(clients, switchablesocket.CmdOff)
		}
	}
}

func switchCondition(clients *Clients, symoData *SymoData, currentSoc float64) bool {
	now := time.Now().UTC()
	sunset := now.Add(-time.Hour)

	openWeatherData := clients.Weather.GetWeatherCached()
	if openWeatherData == nil {
		log.Warn("failed to fetch weather -> disable optimisation")
	} else {
		sunset = time.Unix(openWeatherData.Current.Sunset, 0).UTC()
	}

	limit := 700.0
	if GetAllSwitchesState(clients) {
		limit = -50.0
	}

	result := false
	if now.Before(sunset.Add(-time.Hour)) {
		result = (isGainingEnergy(symoData, limit) && currentSoc > 10) || currentSoc > 50
	} else {
		result = isGainingEnergy(symoData, limit)
	}
	log.Infof("To/From grid: %v, SOC: %v --> results in switch state %v", symoData.Site.PowerGrid, currentSoc, result)
	return result
}

func isGainingEnergy(symoData *SymoData, limit float64) bool {
	return symoData.Site.PowerGrid < -limit || symoData.Site.PowerAccu < -limit
}

func GetTargetSocByCloud(configuration *cfg.Configuration, clouds float64) float64 {
	limitLow := configuration.Optimisation.SocStart
	limitHigh := configuration.Optimisation.SocEnd
	x := clouds / 500
	return math.Min(100, limitLow+(limitHigh-limitLow)*normalizedCurve(x))
}
func curv(x float64) float64 {
	return math.Atan((x*2 - 1) * 3)
}

func normalizedCurve(x float64) float64 {
	return (curv(x)/curv(1) + 1) / 2
}

func GetTargetSoc(now time.Time, begin time.Time, end time.Time, configuration *cfg.Configuration) float64 {
	limitLow := configuration.Optimisation.SocStart
	limitHigh := configuration.Optimisation.SocEnd
	log.Infof("low / high: %v / %v", limitLow, limitHigh)
	return limitHigh - (limitHigh-limitLow)*end.Sub(now).Seconds()/end.Sub(begin).Seconds()
}

type batteryConfig struct {
	HybEmMode  int `json:"HYB_EM_MODE"`
	HybEmPower int `json:"HYB_EM_POWER"`
}
