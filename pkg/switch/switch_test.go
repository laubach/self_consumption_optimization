package switchable_socket

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func Test_GetPowerStateOFF_GivenUrl_WhenRequestData_ThenParseStruct(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("{ \"POWER\": \"OFF\" }"))
	}))

	c, err := NewSwitchClient(ClientOptions{
		URL: server.URL,
	})
	require.NoError(t, err)

	p, err := c.GetPowerState()
	assert.NoError(t, err)
	assert.Equal(t, int64(0), p)
}

func Test_GetPowerStateON_GivenUrl_WhenRequestData_ThenParseStruct(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("{ \"POWER\": \"ON\" }"))
	}))

	c, err := NewSwitchClient(ClientOptions{
		URL: server.URL,
	})
	require.NoError(t, err)

	p, err := c.GetPowerState()
	assert.NoError(t, err)
	assert.Equal(t, int64(1), p)
}

func Test_GetPowerStateUnknown_GivenUrl_WhenRequestData_ThenParseStruct(t *testing.T) {
	server := httptest.NewServer(http.HandlerFunc(func(rw http.ResponseWriter, req *http.Request) {
		rw.Write([]byte("{ \"POWER\": \"OF\" }"))
	}))

	c, err := NewSwitchClient(ClientOptions{
		URL: server.URL,
	})
	require.NoError(t, err)

	p, err := c.GetPowerState()
	assert.NoError(t, err)
	assert.Equal(t, int64(-1), p)
}
