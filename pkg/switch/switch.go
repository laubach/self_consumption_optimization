package switchable_socket

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"net/http"
	"net/url"
	"time"
)

const (
	CacheName = "switch"
	CmdOff    = "Power OFF"
	CmdOn     = "Power ON"
)

type (
	Power struct {
		Power string `json:"POWER"`
	}
	SwitchClient struct {
		request *http.Request
		Options ClientOptions
	}
	ClientOptions struct {
		URL     string
		Headers http.Header
		Timeout time.Duration
	}
)

// NewSwitchClient constructs a SwitchClient ready to use for switching/toggling state of the switch.
func NewSwitchClient(options ClientOptions) (*SwitchClient, error) {
	u, err := url.Parse(options.URL)
	if err != nil {
		return nil, err
	}
	return &SwitchClient{
		request: &http.Request{
			URL:    u,
			Header: options.Headers,
		},
		Options: options,
	}, nil
}

func (c *SwitchClient) GetPowerState() (int64, error) {
	response, err := c.PowerState("Power")
	if err != nil {
		return -1, err
	}
	defer response.Body.Close()
	p := Power{}
	err = json.NewDecoder(response.Body).Decode(&p)
	if err != nil {
		log.Errorf("failed to parse power state: %s", err.Error())
		return -1, err
	}
	log.Debugf("current state is: %v", p.Power)
	return mapPowerState(p.Power), nil
}
func mapPowerState(state string) int64 {
	if state == "ON" {
		return 1
	} else if state == "OFF" {
		return 0
	}
	return -1
}
func (c *SwitchClient) SwitchOn() (*http.Response, error) {
	return c.PowerState(CmdOn)
}
func (c *SwitchClient) SwitchOff() (*http.Response, error) {
	return c.PowerState(CmdOff)
}

func (c *SwitchClient) PowerState(command string) (*http.Response, error) {
	log.Debugf("called with '%s'", command)
	client := http.DefaultClient
	client.Timeout = c.Options.Timeout
	query := c.request.URL.Query()
	query.Del("cmnd")
	query.Add("cmnd", command)
	c.request.URL.RawQuery = query.Encode()
	log.Debugf("url: %v", c.request)
	response, err := client.Do(c.request)
	if err != nil {
		log.Errorf("failed to query for power state: %s", err.Error())
		return nil, err
	}
	return response, nil
}
